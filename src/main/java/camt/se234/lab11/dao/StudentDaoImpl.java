package camt.se234.lab11.dao;

import camt.se234.lab11.entity.Student;

import java.util.ArrayList;
import java.util.List;

public class StudentDaoImpl implements StudentDao {
    List<Student> students;
    public StudentDaoImpl(){
        students = new ArrayList<>();
        students.add(new Student("001","Jon","Snow",2.33));
        students.add(new Student("002","Arya","Stark",3.89));
        students.add(new Student("003","Jaime","Lannister",2.88));
        students.add(new Student("004","Daenerys","Targaryen",1.46));
        students.add(new Student("005","Jaqen","H'ghar",0.45));

    }

    public void studentSet2(){
        students = null;
        students = new ArrayList<>();
        students.add(new Student("001","Han","Solo",2.39));
        students.add(new Student("002","Darth","Vader",3.50));
        students.add(new Student("003","Luke","Skywalker",1.33));
        students.add(new Student("004","Obiwan","Kenobi",0.88));
        students.add(new Student("005","R2","D2",4.00));
    }

    @Override
    public List<Student> findAll() {
        return this.students;
    }
}
